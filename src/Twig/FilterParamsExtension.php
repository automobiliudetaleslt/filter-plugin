<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Twig;

use Symfony\Component\HttpFoundation\Request;

class FilterParamsExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('omni_filter_get_reset_query_params', [$this, 'getResetQueryParams']),
            new \Twig_SimpleFunction('omni_filter_get_reset_all_query_params', [$this, 'getResetAllParams']),
            new \Twig_SimpleFunction('omni_filter_get_reset_price_query_params', [$this, 'getResetPriceParams']),
            new \Twig_SimpleFunction('omni_filter_get_reset_single_choice_query_params', [$this, 'getResetSingleChoice']),
        );
    }

    /**
     * @param Request $request
     * @param string  $attributeName
     *
     * @return array
     */
    public function getResetQueryParams(Request $request, string $attributeName): array
    {
        $params = $request->query->all();
        $params['slug'] = $request->attributes->get('slug');
        unset($params['criteria']['attributes'][$attributeName]);

        return $params;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function getResetAllParams(Request $request): array
    {
        $params = $request->query->all();
        $params['slug'] = $request->attributes->get('slug');
        unset($params['criteria']);

        return $params;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function getResetPriceParams(Request $request): array
    {
        $params = $request->query->all();
        $params['slug'] = $request->attributes->get('slug');
        unset($params['criteria']['price']);

        return $params;
    }

    /**
     * @param Request $request
     * @param string $attributeName
     * @param string $choiceName
     *
     * @return array
     */
    public function getResetSingleChoice(Request $request, string $attributeName, string $choiceName): array
    {
        $params = $request->query->all();
        $params['slug'] = $request->attributes->get('slug');

        if (false === isset($params['criteria']['attributes'][$attributeName])) {
            return $params;
        }

        foreach ($params['criteria']['attributes'][$attributeName] as $key => $name) {
            if ($name === $choiceName) {
                unset($params['criteria']['attributes'][$attributeName][$key]);

                return $params;
            }
        }

        return $params;
    }
}
