<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Indexer;

use Sylius\Component\Attribute\Model\Attribute;
use Sylius\Component\Product\Model\ProductInterface;
use Symfony\Component\PropertyAccess\Exception;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class TagManager
{
    const TAG_SEPARATOR = '|';

    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * @param PropertyAccessor $accessor
     *
     * @throws \Exception
     */
    public function __construct(PropertyAccessor $accessor)
    {
        $this->accessor = $accessor;
    }

    /**
     * @param object      $element
     * @param Attribute[] $attributes
     *
     * @return array
     */
    public function extractTags($element, $attributes)
    {
        $result = [];

        foreach ($attributes as $tag => $attribute) {
            $values = $this->getValue($element, $tag);
            if (is_iterable($values)) {
                foreach ($values as $value) {
                    if (is_array($value)) {
                        foreach ($value as $valueItem) {
                            $result[$tag][] = trim($valueItem);
                        }
                    } else {
                        $result[$tag][] = trim($value);
                    }
                }
            } elseif (null !== $values) {
                $result[$tag] = trim($values);
            }
        }

        return $result;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function buildTagIndex($data)
    {
        $result = '';
        foreach ($data as $key => $values) {
            $values = is_array($values) ? $values : [$values];
            foreach ($values as $value) {
                $result .= sprintf('%s%s:%s%s', self::TAG_SEPARATOR, $key, (string)$value, self::TAG_SEPARATOR);
            }
        }

        return $result;
    }

    private function getValue($product, $propertyPath)
    {
        try {
            return $this->accessor->getValue($product, $propertyPath);
        } catch (Exception\NoSuchPropertyException $e) {
            $tags = [];
            if (!$product instanceof ProductInterface) {
                return $tags;
            }

            $propertyPath = strtolower((string)$propertyPath);
            foreach ($product->getVariants() as $variant) {
                foreach ($variant->getOptionValues() as $option) {
                    if ($propertyPath === strtolower($option->getOptionCode())) {
                        $this->appendValue($tags, $option->getValue());
                    }
                }
            }

            foreach ($product->getAttributes() as $attribute) {
                if ($propertyPath === strtolower($attribute->getCode())) {
                    $this->appendValue($tags, $attribute->getValue());
                }
            }

            if (isset($tags[0]) && is_array($tags[0])) {
                $tags = call_user_func_array('array_merge', $tags);
            }

            return array_values(array_unique($tags));
        }
    }

    /**
     * @param array $tags
     * @param mixed $value
     */
    private function appendValue(array &$tags, $value): void
    {
        if (is_array($value)) {
            $tags = array_merge($tags, $value);
        } else {
            $tags[] = $value;
        }
    }
}
